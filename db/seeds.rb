# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#AdminUser.find_or_create_by(email: 'shamsheg@gmail.com', password: 'Thecomputer2.', password_confirmation: 'Thecomputer2.')
FixedContent.destroy_all
FixedContent.find_or_create_by(key_name: 'my_name', is_text: true, content:"Shams Hegab.")
FixedContent.find_or_create_by(key_name: 'intro_title_p1', is_text: true, content:"Hello, my name is ")
FixedContent.find_or_create_by(key_name: 'intro_title_p2', is_text: true, content:"I'm a Software Engineer.")

FixedContent.find_or_create_by(key_name: 'about_me_p1', is_text: true, content:" I'm a Full-Stack Developer for HDBC in Alexandria, EG.")
FixedContent.find_or_create_by(key_name: 'about_me_p2', is_text: true, content:"Demonstrated skill working in different segments. Skilled in Ruby on Rails, Java, Python, PHP, JavaScript and Native mobile development. Strong in Git and Linux. Having a tracked record of leadership positions.")
FixedContent.find_or_create_by(key_name: 'about_me_pic', is_text: false, content: "img")
FixedContent.find_or_create_by(key_name: 'about_me_p3', is_text: true, content: "I have a serious passion for creating dynamic, animated and intuitive web experiences.")

FixedContent.find_or_create_by(key_name: 'fb', is_text: true, content: "https://www.facebook.com/profile.php?id=100007173215108")
FixedContent.find_or_create_by(key_name: 'li', is_text: true, content: "https://www.linkedin.com/in/shams-hegab/")
FixedContent.find_or_create_by(key_name: 'email', is_text: true, content: "shamsheg@gmail.com")
FixedContent.find_or_create_by(key_name: 'insta', is_text: true, content: "https://www.instagram.com/shams_hegab/")
FixedContent.find_or_create_by(key_name: 'resume', is_text: false, content: "cv")
