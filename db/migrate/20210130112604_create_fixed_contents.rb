class CreateFixedContents < ActiveRecord::Migration[6.0]
  def change
    create_table :fixed_contents do |t|

      t.timestamps
    end
  end
end
