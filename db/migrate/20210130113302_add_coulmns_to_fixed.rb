class AddCoulmnsToFixed < ActiveRecord::Migration[6.0]
  def change
    add_column :fixed_contents, :key_name, :string
    add_column :fixed_contents, :is_text, :boolean
    add_column :fixed_contents, :content, :text
    add_column :fixed_contents, :img, :string
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
