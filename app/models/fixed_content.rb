# == Schema Information
#
# Table name: fixed_contents
#
#  id         :bigint           not null, primary key
#  content    :text
#  img        :string
#  is_text    :boolean
#  key_name   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class FixedContent < ApplicationRecord
    mount_uploader :img, ImageUploader
end
