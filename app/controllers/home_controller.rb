class HomeController < ApplicationController
  def index
    @projects=Project.all
    @my_name=FixedContent.find_by(key_name: "my_name")
    @intro_title_p1=FixedContent.find_by(key_name: "intro_title_p1")
    @intro_title_p2=FixedContent.find_by(key_name: "intro_title_p2")
    @about_me_p1=FixedContent.find_by(key_name: "about_me_p1")
    @about_me_p2=FixedContent.find_by(key_name: "about_me_p2")
    @about_me_p3=FixedContent.find_by(key_name: "about_me_p3")
    @about_me_pic=FixedContent.find_by(key_name: "about_me_pic")

    @fb=FixedContent.find_by(key_name: "fb")
    @li=FixedContent.find_by(key_name: "li")
    @insta=FixedContent.find_by(key_name: "insta")
    @resume=FixedContent.find_by(key_name: "resume")
    @email=FixedContent.find_by(key_name: "email")

  end

  def show
    p "----------------"
    @project = Project.find(params[:id])
    p @project
    render json: @project
    
  end

  def create
    p params
    m=Message.new(name: params[:name], email: params[:email], msg: params[:msg])
    if m.save
      render json: {
        msg: "Your message was sent. I will get back to you soon.",
        status: 202
      },status: 202
    else
      render json: {
        msg: "I am sorry something wrong has happend. Please try again.",
        status: 500
      },status: 500
    end
  end
end
