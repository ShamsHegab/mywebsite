ActiveAdmin.register Message do
    permit_params :naem, :email, :msg
  
    index do
      selectable_column
      id_column
      column :name
      column :email
      column :created_at
      actions
    end
  
    filter :name
    filter :email
    filter :created_at
   
  
    
  
  end
  