ActiveAdmin.register FixedContent do
    permit_params :key_name, :content, :img, :is_text
  
    index do
      selectable_column
      id_column
      column :key_name
      actions
    end
  
    filter :key_name
    filter :is_text
    filter :created_at
    filter :updated_at
  
    form :html => { :enctype => "multipart/form-data" } do |f|
      f.inputs do
        f.input :key_name
        f.input :content
        f.input :is_text
        f.input :img, :as => :file
      end
      f.actions
    end
  
  end
  